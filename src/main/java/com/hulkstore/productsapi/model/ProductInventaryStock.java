package com.hulkstore.productsapi.model;

import com.hulkstore.productsapi.domain.MovementKind;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "product_inventary")
public class ProductInventaryStock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUCT_INVENTARY_ID")
    private Integer productInventaryId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID",nullable = false)
    private Product product;

    @Column(name = "PRODUCT_INVENTARY_DATE_CREATED")
    private LocalDateTime productInventaryDateCreated;

    @Column(name = "PRODUCT_INVENTARY_DATE_LAST_UPDATE")
    private LocalDateTime productInventaryLastUpdate;

    @Column(name = "PRODUCT_INVENTARY_DETAIL")
    private MovementKind movementKind;

    @Column(name = "PRODUCT_INVENTARY_EXIST_QTY")
    private Integer productInventaryExistQty;

    @Column(name = "PRODUCT_INVENTARY_EXIST_UNITPRICE")
    private BigDecimal productInventaryExistUnitPrice;

    @Column(name = "PRODUCT_INVENTARY_EXIST_TOTAL")
    private BigDecimal productInventaryExistTotal;

    @Column(name = "PRODUCT_INVENTARY_INFLOW_QTY")
    private Integer productInventaryInflowQty;

    @Column(name = "PRODUCT_INVENTARY_INFLOW_UNITPRICE")
    private BigDecimal productInventaryInflowUnitPrice;

    @Column(name = "PRODUCT_INVENTARY_INFLOW_TOTAL")
    private BigDecimal productInventaryInflowTotal;

    @Column(name = "PRODUCT_INVENTARY_OUTFLOW_QTY")
    private Integer productInventaryOutflowQty;

    @Column(name = "PRODUCT_INVENTARY_OUTFLOW_UNITPRICE")
    private BigDecimal productInventaryOutflowUnitPrice;

    @Column(name = "PRODUCT_INVENTARY_OUTFLOW_TOTAL")
    private BigDecimal productInventaryOutflowTotal;

}
