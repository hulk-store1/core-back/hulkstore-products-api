package com.hulkstore.productsapi.domain;

public enum MovementKind {
    InitialInventary("Inventario inicial"),
    Buy("Compras"),
    Sale("Ventas");

    public final String label;

    private MovementKind(String label) {
        this.label = label;
    }
}
