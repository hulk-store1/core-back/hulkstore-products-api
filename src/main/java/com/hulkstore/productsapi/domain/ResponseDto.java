package com.hulkstore.productsapi.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;

@NoArgsConstructor
@Getter
public class ResponseDto<T> {

    private OffsetDateTime timestamp;
    private int status;
    private String responseCode;
    private String responseMessage;
    private List<FieldErrorDto> fieldErrors;
    private T data;

    public ResponseDto(OffsetDateTime timestamp, int status, String responseCode, String responseMessage, List<FieldErrorDto> fieldErrors, T data) {
        this.timestamp = timestamp;
        this.status = status;
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.fieldErrors = fieldErrors;
        this.data = data;
    }

    public ResponseDto(int status, String responseCode, String responseMessage, List<FieldErrorDto> fieldErrors) {
        this(OffsetDateTime.now(), status, responseCode, responseMessage, fieldErrors, null);
    }

    public ResponseDto(int status, String responseCode, String responseMessage, T data, List<FieldErrorDto> fieldErrors) {
        this(OffsetDateTime.now(), status, responseCode, responseMessage, null, data);
    }

    public ResponseDto(int status, String responseCode, String responseMessage, T data) {
        this(OffsetDateTime.now(), status, responseCode, responseMessage, null, data);
    }

    public ResponseDto(int status, String responseCode, String responseMessage) {
        this(OffsetDateTime.now(), status, responseCode, responseMessage, null, null);

    }

    @Override
    public String toString() {
        return "ResponseDto [status=" + status + ", responseCode=" + responseCode + ", responseMessage=" + responseMessage
                + ", fieldErrors=" + fieldErrors + ", data=" + data + "]";
    }

}
