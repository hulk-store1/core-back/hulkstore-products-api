package com.hulkstore.productsapi.domain;

import com.hulkstore.productsapi.exception.ApiResponseObject;
import com.hulkstore.productsapi.util.MessagesUtil;
import com.hulkstore.productsapi.util.ValidationUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.OffsetDateTime;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class AccountExceptionHandler {

    private final MessagesUtil messagesUtil;
    private final ValidationUtils validationUtils;

    @ExceptionHandler({ProductsException.class})
    public ResponseEntity<ApiResponseObject> handleProductException(ProductsException e) {
        ApiResponseObject response = ApiResponseObject.builder()
                .timestamp(OffsetDateTime.now())
                .status(e.getStatus())
                .code(e.getCode())
                .message(e.getMessage())
                .fields(e.getFieldErrors())
                .path(e.getPath()).build();
        return new ResponseEntity<>(response, HttpStatus.valueOf(e.getStatus()));
    }


}
