package com.hulkstore.productsapi.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class ProductInventaryStockDto {

    private LocalDateTime date;
    private MovementKind movement;
    private InFlowDto inFlowDto;
    private OutFlowDto outFlowDto;
    private ExistDto existDto;

    /**
     * @apiNote Dto, representa Ingresos de inventario en la tabla
     * */
    @Data @Builder @NoArgsConstructor @AllArgsConstructor
    public static class InFlowDto {
        @JsonProperty("inflowInventaryQty")
        private Integer productInventaryInflowQty;
        @JsonProperty("inflowUnitPrice")
        private BigDecimal productInventaryInflowUnitPrice;
        @JsonProperty("inflowTotalPrice")
        private BigDecimal productInventaryInflowTotal;
    }


    /**
     * @apiNote Dto, representa Egresos de inventario en la tabla
     * */
    @Data @Builder @NoArgsConstructor @AllArgsConstructor
    public static class OutFlowDto {
        @JsonProperty("outflowInventaryQty")
        private Integer productInventaryOutflowQty;
        @JsonProperty("outflowUnitPrice")
        private BigDecimal productInventaryOutflowUnitPrice;
        @JsonProperty("outflowTotalPrice")
        private BigDecimal productInventaryOutflowTotal;
    }


    /**
     * @apiNote Dto, representa Existencia actual de inventario
     * */
    @Data @Builder @NoArgsConstructor @AllArgsConstructor
    public static class ExistDto {
        @JsonProperty("existInventaryQty")
        private Integer productInventaryExistQty;
        @JsonProperty("existUnitPrice")
        private BigDecimal productInventaryExistUnitPrice;
        @JsonProperty("existTotalPrice")
        private BigDecimal productInventaryExistTotal;
    }

}
