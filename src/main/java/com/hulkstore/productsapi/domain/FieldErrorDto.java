package com.hulkstore.productsapi.domain;

import lombok.Value;

import java.io.Serial;
import java.io.Serializable;

@Value
public class FieldErrorDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -8619947059150751384L;
    String field;
    String message;

}
