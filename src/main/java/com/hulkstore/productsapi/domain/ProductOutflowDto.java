package com.hulkstore.productsapi.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Builder @AllArgsConstructor @NoArgsConstructor
public class ProductOutflowDto {

    @JsonProperty("outflowInventaryProduct")
    private ProductInventaryStockDto.OutFlowDto outFlowDto;

}
