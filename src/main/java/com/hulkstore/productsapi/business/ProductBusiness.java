package com.hulkstore.productsapi.business;

import com.hulkstore.productsapi.domain.*;
import com.hulkstore.productsapi.model.ProductInventaryStock;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProductBusiness {

    /**
     * @apiNote Obtiene listado de movimientos de un producto, desde su creacion como entradas y salidas de inventario
     * @param token: token de autenticacion
     * @param idProduct: id de producto a obtener listaedo de movimientos
     * @return ResponseEntity: Listado de productos
     */
    ResponseEntity<?> getProductKardex(String token, String idProduct);


    /**
     * @apiNote Creacion o actualizacion de producto en el sistema kardex.
     * @param token: token de autenticacion
     * @param idProduct: id de producto a obtener listado de movimientos, si su valor es cero, lo crea y si trae un producto existente lo edita.
     * @param productCreateDto: Dto con informacion relevante para la creacion del producto, nombre, imagen, inventario de entrada y precio unitario
     * @return ResponseEntity: 201 Status Created producto creado correctamente
     */
    ResponseEntity<?> createProductKardex(String token, String idProduct, ProductCreateDto productCreateDto);


    /**
     * @apiNote Ingreso de inventario en el sistema kardex para el id del producto solicitado.
     * @param token: token de autenticacion
     * @param idProduct: id de producto a ingresar inventario.
     * @param productInflowDto: Dto con informacion para el ingreso de inventario, inventario a ingresar, precio unitario de compra.
     * @return ResponseEntity: 200 Status OK movimiento creado correctamente
     */
    ResponseEntity<?> createInflowInventaryKardex(String token, String idProduct, ProductInflowDto productInflowDto);


    /**
     * @apiNote Egreso de inventario en el sistema kardex para el id del producto solicitado.
     * @param token: token de autenticacion
     * @param idProduct: id de producto a ingresar inventario.
     * @param productOutflowDto: Dto con informacion para el egreso de inventario, inventario a extraer.
     * @return ResponseEntity: 200 Status OK movimiento creado correctamente
     */
    ResponseEntity<ResponseDto<ProductInventaryStock>> createOutflowInventaryKardex(String token, String idProduct, ProductOutflowDto productOutflowDto);


    /**
     * @apiNote Obtener listado de productos
     * @param token: token de autenticacion
     * @return ResponseEntity: 200 Status OK listado de productos retornados.
     */
    ResponseEntity<ResponseDto<List<ProductDto>>> getProducts(String token);

}
