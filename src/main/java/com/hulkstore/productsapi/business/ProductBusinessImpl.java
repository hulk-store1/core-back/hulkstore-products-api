package com.hulkstore.productsapi.business;


import com.hulkstore.productsapi.domain.*;
import com.hulkstore.productsapi.model.Product;
import com.hulkstore.productsapi.model.ProductInventaryStock;
import com.hulkstore.productsapi.repository.ProductInventaryStockRepository;
import com.hulkstore.productsapi.repository.ProductRepository;
import com.hulkstore.productsapi.util.MessagesUtil;
import com.hulkstore.productsapi.util.UtilConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class ProductBusinessImpl implements ProductBusiness{

    // Capa de persistencia, conexion db tabla "products"
    private final ProductRepository productRepository;

    // Capa de persistencia, conexion db tabla "products_inventary"
    private final ProductInventaryStockRepository productInventaryStockRepository;

    // Componente helper para formatear mensajes
    private final MessagesUtil messagesUtil;

    @Override
    public ResponseEntity<?> getProductKardex(String token, String idProduct) {
        log.info("Inicio getProducts");

        idProduct = !(StringUtils.isBlank(idProduct))? idProduct: "0";
        Integer idProductInteger = Integer.valueOf(idProduct);

        Optional<Product> productReturned = getProductById(idProductInteger);

        List<ProductInventaryStock> productInventaryStockList = getMovementsFromProduct(idProductInteger);

        List<ProductInventaryStockDto> productInventaryStockDtoList = getMovementsTranslatedToDto(productInventaryStockList);

        ResponseDto responseDto = new ResponseDto(HttpStatus.OK.value(), UtilConstants.DAA000,
                messagesUtil.getMessage(UtilConstants.DAA000), productInventaryStockDtoList, null);
        log.info("Fin getProducts");
        return new ResponseEntity<>(responseDto, HttpStatus.resolve(responseDto.getStatus()));
    }

    /**
     * @apiNote Convierte instancia de entidad a DTO de respuesta
     * */
    private List<ProductInventaryStockDto> getMovementsTranslatedToDto(List<ProductInventaryStock> productInventaryStockList) {
        return productInventaryStockList.stream().map(productInventaryStock -> ProductInventaryStockDto.builder()
                .date(productInventaryStock.getProductInventaryLastUpdate())
                .movement(productInventaryStock.getMovementKind())
                .inFlowDto(
                        ProductInventaryStockDto.InFlowDto.builder()
                                .productInventaryInflowQty(productInventaryStock.getProductInventaryInflowQty())
                                .productInventaryInflowUnitPrice(productInventaryStock.getProductInventaryInflowUnitPrice())
                                .productInventaryInflowTotal(productInventaryStock.getProductInventaryInflowTotal())
                                .build()
                )
                .outFlowDto(
                        ProductInventaryStockDto.OutFlowDto.builder()
                                .productInventaryOutflowQty(productInventaryStock.getProductInventaryOutflowQty())
                                .productInventaryOutflowUnitPrice(productInventaryStock.getProductInventaryOutflowUnitPrice())
                                .productInventaryOutflowTotal(productInventaryStock.getProductInventaryOutflowTotal())
                                .build()
                )
                .existDto(
                        ProductInventaryStockDto.ExistDto.builder()
                                .productInventaryExistQty(productInventaryStock.getProductInventaryExistQty())
                                .productInventaryExistUnitPrice(productInventaryStock.getProductInventaryExistUnitPrice())
                                .productInventaryExistTotal(productInventaryStock.getProductInventaryExistTotal())
                                .build()
                )
                .build()).collect(Collectors.toList());
    }

    /**
     * @apiNote Obtiene los movimientos de productos
     * */
    private List<ProductInventaryStock> getMovementsFromProduct(Integer idProduct) {
        Optional<Product> product = productRepository.findById(idProduct);
        if(product.isEmpty()) {
            log.error("Producto con id {} no encontrado.", idProduct);
            //lanzar exception
        }

        List<ProductInventaryStock> inventaryStockList =  productInventaryStockRepository.findByProduct(product.get());
        if(inventaryStockList.isEmpty()) {
            log.error("Producto con id {} no cuenta con movimientos.", idProduct);
            //lanzar exception
        }
        return inventaryStockList;
    }

    @Override
    public ResponseEntity<?> createProductKardex(String token, String idProduct, ProductCreateDto productCreateDto) {
        try{
            ResponseDto responseDto = new ResponseDto();
            idProduct = !(StringUtils.isBlank(idProduct))? idProduct: "0";
            Integer idProductInteger = Integer.valueOf(idProduct);

            // Creacion de producto si idProduct = 0
            if(idProductInteger == 0) {
                Product productSaved = saveNewProductWithInventary(productCreateDto);
                responseDto = new ResponseDto(HttpStatus.CREATED.value(), UtilConstants.DAA001, messagesUtil.getMessage(UtilConstants.DAA001),
                        "id nuevo producto: " + productSaved.getProductId(), null);
            } else { //editar producto si es un id diferente y si existe
                Optional<Product> productReturned = getProductById(idProductInteger);
                Product product = new Product();
                product.setProductId(productReturned.get().getProductId());
                product.setProductName(productCreateDto.getProductName());
                product.setProductImage(productCreateDto.getProductImage());
                Product productSaved = productRepository.save(product);
                responseDto = new ResponseDto(HttpStatus.NO_CONTENT.value(), UtilConstants.DAA002,
                        messagesUtil.getMessage(UtilConstants.DAA002), null, null);
            }

            return new ResponseEntity<>(responseDto, HttpStatus.valueOf(responseDto.getStatus()));

        } catch (IllegalArgumentException | NullPointerException e) {
            log.error("El id del producto solicitado {} no es un valor valido.", idProduct);
            throw new ProductsException(HttpStatus.BAD_REQUEST.value(), UtilConstants.DAE010,
                    messagesUtil.getFormatedMessage(UtilConstants.DAE010, idProduct));
        } catch (Exception e) {
            log.error(messagesUtil.getMessage(UtilConstants.DAE000));
            throw e;
        }
    }

    @Override
    public ResponseEntity<?> createInflowInventaryKardex(String token, String idProduct, ProductInflowDto productInflowDto) {

        try {
            idProduct = !(StringUtils.isBlank(idProduct))? idProduct: "0";
            Integer idProductInteger = Integer.valueOf(idProduct);

            Optional<Product> productReturned = getProductById(idProductInteger);

            List<ProductInventaryStock> productInventaryStockList = productInventaryStockRepository
                    .findByProductOrderByProductInventaryLastUpdateDesc(productReturned.get());

            ProductInventaryStock productInventaryStock = new ProductInventaryStock();
            productInventaryStock.setProduct(productReturned.get());
            productInventaryStock.setMovementKind(MovementKind.Buy);
            productInventaryStock.setProductInventaryDateCreated(LocalDateTime.now());
            productInventaryStock.setProductInventaryLastUpdate(LocalDateTime.now());
            productInventaryStock.setProductInventaryInflowQty(productInflowDto.getInFlowDto().getProductInventaryInflowQty());
            productInventaryStock.setProductInventaryInflowUnitPrice(productInflowDto.getInFlowDto().getProductInventaryInflowUnitPrice());
            productInventaryStock.setProductInventaryInflowTotal(BigDecimal.valueOf(productInflowDto.getInFlowDto().getProductInventaryInflowQty())
                    .multiply(productInflowDto.getInFlowDto().getProductInventaryInflowUnitPrice()));
            productInventaryStock.setProductInventaryExistQty(
                    productInventaryStockList.get(0).getProductInventaryExistQty() + productInflowDto.getInFlowDto().getProductInventaryInflowQty()
            );
            productInventaryStock.setProductInventaryExistTotal(
                    productInventaryStockList.get(0).getProductInventaryExistTotal().add(productInventaryStock.getProductInventaryInflowTotal()));
            productInventaryStock.setProductInventaryExistUnitPrice(
                    productInventaryStock.getProductInventaryExistTotal().divide(BigDecimal.valueOf(productInventaryStock.getProductInventaryExistQty()), 2, RoundingMode.HALF_UP)
            );
            ProductInventaryStock productInventaryStockSaved = productInventaryStockRepository.save(productInventaryStock);
            ResponseDto responseDto = new ResponseDto(HttpStatus.CREATED.value(), UtilConstants.DAA001, messagesUtil.getMessage(UtilConstants.DAA001),
                    productInventaryStockSaved, null);
            return new ResponseEntity<ResponseDto<ProductInventaryStock>>(responseDto, HttpStatus.resolve(responseDto.getStatus()));

        } catch (Exception e) {
            log.debug(messagesUtil.getMessage(UtilConstants.DAE000));
            throw e;
        }
    }

    private Product saveNewProductWithInventary(ProductCreateDto productCreateDto) {

        Product product = new Product();
        product.setProductName(productCreateDto.getProductName());
        product.setProductImage(productCreateDto.getProductImage());
        Product productSaved = productRepository.save(product);

        ProductInventaryStock productInventaryStock = new ProductInventaryStock();
        productInventaryStock.setProduct(productSaved);
        productInventaryStock.setProductInventaryLastUpdate(LocalDateTime.now());
        productInventaryStock.setProductInventaryDateCreated(LocalDateTime.now());
        productInventaryStock.setMovementKind(MovementKind.InitialInventary);
        productInventaryStock.setProductInventaryExistQty(productCreateDto.getExistDto().getProductInventaryExistQty());
        productInventaryStock.setProductInventaryExistUnitPrice(productCreateDto.getExistDto().getProductInventaryExistUnitPrice());
        productInventaryStock.setProductInventaryExistTotal(BigDecimal.valueOf(productCreateDto.getExistDto().getProductInventaryExistQty())
                .multiply(productCreateDto.getExistDto().getProductInventaryExistUnitPrice()));
        ProductInventaryStock productInventaryStockSaved = productInventaryStockRepository.save(productInventaryStock);

        return productSaved;
    }

    @Override
    public ResponseEntity<ResponseDto<ProductInventaryStock>> createOutflowInventaryKardex(String token, String idProduct, ProductOutflowDto productOutflowDto) {

        try {
            idProduct = !(StringUtils.isBlank(idProduct))? idProduct: "0";
            Integer idProductInteger = Integer.valueOf(idProduct);

            Optional<Product> productReturned = getProductById(idProductInteger);

            List<ProductInventaryStock> productInventaryStockList = productInventaryStockRepository
                    .findByProductOrderByProductInventaryLastUpdateDesc(productReturned.get());

            if(productInventaryStockList.get(0).getProductInventaryExistQty() < productOutflowDto.getOutFlowDto().getProductInventaryOutflowQty()) {
                log.debug(messagesUtil.getFormatedMessage(UtilConstants.DAE011, idProduct));
                throw new ProductsException(HttpStatus.BAD_REQUEST.value(), UtilConstants.DAE011, messagesUtil.getFormatedMessage(UtilConstants.DAE011, idProduct));
            }

            ProductInventaryStock productInventaryStock = new ProductInventaryStock();
            productInventaryStock.setProduct(productReturned.get());
            productInventaryStock.setMovementKind(MovementKind.Sale);
            productInventaryStock.setProductInventaryDateCreated(LocalDateTime.now());
            productInventaryStock.setProductInventaryLastUpdate(LocalDateTime.now());
            productInventaryStock.setProductInventaryOutflowQty(productOutflowDto.getOutFlowDto().getProductInventaryOutflowQty());
            productInventaryStock.setProductInventaryOutflowUnitPrice(productInventaryStockList.get(0).getProductInventaryExistUnitPrice());
            productInventaryStock.setProductInventaryOutflowTotal(BigDecimal.valueOf(productOutflowDto.getOutFlowDto().getProductInventaryOutflowQty())
                    .multiply(productInventaryStockList.get(0).getProductInventaryExistUnitPrice()));
            productInventaryStock.setProductInventaryExistQty(
                    productInventaryStockList.get(0).getProductInventaryExistQty() - productOutflowDto.getOutFlowDto().getProductInventaryOutflowQty()
            );
            productInventaryStock.setProductInventaryExistTotal(
                    productInventaryStockList.get(0).getProductInventaryExistTotal().subtract(productInventaryStock.getProductInventaryOutflowTotal())
            );
            productInventaryStock.setProductInventaryExistUnitPrice(
                    productInventaryStock.getProductInventaryExistTotal().divide(BigDecimal.valueOf(
                            (productInventaryStock.getProductInventaryExistQty()) == 0? 1:productInventaryStock.getProductInventaryExistQty()
                    ), 2, RoundingMode.HALF_UP)
            );
            ProductInventaryStock productInventaryStockSaved = productInventaryStockRepository.save(productInventaryStock);
            ResponseDto responseDto = new ResponseDto(HttpStatus.CREATED.value(), UtilConstants.DAA001, messagesUtil.getMessage(UtilConstants.DAA001),
                    productInventaryStockSaved, null);
            return new ResponseEntity<ResponseDto<ProductInventaryStock>>(responseDto, HttpStatus.resolve(responseDto.getStatus()));

        } catch (Exception e) {
            log.debug(messagesUtil.getMessage(UtilConstants.DAE000));
            throw e;
        }
    }


    /**
     * @apiNote Obtiene producto segun su id
     * */
    private Optional<Product> getProductById(Integer idProductInteger) {
        Optional<Product> productReturned = productRepository.findById(idProductInteger);
        if(productReturned.isEmpty()) {
            log.debug(messagesUtil.getFormatedMessage(UtilConstants.DAE010, idProductInteger));
            throw new ProductsException(HttpStatus.BAD_REQUEST.value(), UtilConstants.DAE010, messagesUtil.getFormatedMessage(UtilConstants.DAE010, idProductInteger));
        }
        return productReturned;
    }

    @Override
    public ResponseEntity<ResponseDto<List<ProductDto>>> getProducts(String token) {
        try {
            log.info("Inicio getProducts");
            List<Product> products = productRepository.findAll();
            if(products.isEmpty()) {
                log.debug(messagesUtil.getMessage(UtilConstants.DAE012));
                throw new ProductsException(HttpStatus.NOT_FOUND.value(), UtilConstants.DAE012, messagesUtil.getMessage(UtilConstants.DAE012));
            }
            List<ProductDto> productDtoList = products.stream().map(product -> {
                ProductDto productDto = new ProductDto();
                BeanUtils.copyProperties(product, productDto);
                return productDto;
            }).collect(Collectors.toList());

            ResponseDto responseDto = new ResponseDto(HttpStatus.OK.value(), UtilConstants.DAA003,
                    messagesUtil.getMessage(UtilConstants.DAA003), productDtoList, null);
            log.info("Fin getProducts");
            return new ResponseEntity<>(responseDto, HttpStatus.resolve(responseDto.getStatus()));
        } catch (Exception e) {
            log.debug(messagesUtil.getMessage(UtilConstants.DAE000));
            throw e;
        }
    }
}


