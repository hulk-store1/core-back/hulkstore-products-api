package com.hulkstore.productsapi.exception;

import com.hulkstore.productsapi.domain.FieldErrorDto;
import lombok.Builder;
import lombok.Data;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@Builder
public class ApiResponseObject {
    private OffsetDateTime timestamp;
    private Integer status;
    private String code;
    private String message;
    private String path;
    private List<FieldErrorDto> fields;
}
