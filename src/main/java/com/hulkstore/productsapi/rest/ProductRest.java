package com.hulkstore.productsapi.rest;

import com.hulkstore.productsapi.business.ProductBusiness;
import com.hulkstore.productsapi.domain.*;
import com.hulkstore.productsapi.model.ProductInventaryStock;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1")
@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT})
@Tag(name = "ProductRest", description = "Api REST encargado de la administracion de productos y movimientos de inventario")
public class ProductRest {

    private final ProductBusiness productBusiness;

    @GetMapping(value = "/product", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<List<ProductDto>>> getProducts(
            @RequestHeader(value = "Authorization", defaultValue = "") String token
    ) {
        return productBusiness.getProducts(token);
    }

    @GetMapping(value = "/product/{idProduct}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getProductKardex(
            @RequestHeader(value = "Authorization", defaultValue = "") String token,
            @PathVariable(value = "idProduct") String idProduct
    ) {
        return productBusiness.getProductKardex(token, idProduct);
    }

    @PutMapping(value = "/product/{idProduct}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createProductKardex(
            @RequestHeader(value = "Authorization", defaultValue = "") String token,
            @PathVariable(value = "idProduct") String idProduct,
            @RequestBody ProductCreateDto productCreateDto
    ) {
        return productBusiness.createProductKardex(token, idProduct, productCreateDto);
    }

    @PutMapping(value = "/product-inflow/{idProduct}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createInflowInventaryKardex(
            @RequestHeader(value = "Authorization", defaultValue = "") String token,
            @PathVariable(value = "idProduct") String idProduct,
            @RequestBody ProductInflowDto productInflowDto
    ) {
        return productBusiness.createInflowInventaryKardex(token, idProduct, productInflowDto);
    }

    @PutMapping(value = "/product-outflow/{idProduct}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<ProductInventaryStock>> createOutflowInventaryKardex(
            @RequestHeader(value = "Authorization", defaultValue = "") String token,
            @PathVariable(value = "idProduct") String idProduct,
            @RequestBody ProductOutflowDto productOutflowDto
    ) {
        return productBusiness.createOutflowInventaryKardex(token, idProduct, productOutflowDto);
    }

}
