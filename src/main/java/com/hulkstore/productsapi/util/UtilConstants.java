package com.hulkstore.productsapi.util;

public class UtilConstants {

    public static final String DAA000 = "DAA000";
    public static final String DAA001 = "DAA001";
    public static final String DAA002 = "DAA002";
    public static final String DAA003 = "DAA003";
    public static final String DAA004 = "DAA004";

    public static final String DAE000 = "DAE000";
    public static final String DAE001 = "DAE001";
    public static final String DAE002 = "DAE002";
    public static final String DAE003 = "DAE003";
    public static final String DAE010 = "DAE010";
    public static final String DAE011 = "DAE011";
    public static final String DAE012 = "DAE012";

}
