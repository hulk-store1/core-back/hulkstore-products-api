package com.hulkstore.productsapi.util;

import com.hulkstore.productsapi.domain.FieldErrorDto;
import com.hulkstore.productsapi.domain.ProductsException;
import com.hulkstore.productsapi.domain.ResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ValidationUtils {

    private final MessagesUtil messagesUtil;
    private final Validator validator;

    /**
     * Ejecuta la validacion de Beans con Hibernate Validation y lanza una excepcion con las violaciones encontradas
     *
     * @param o
     *            Objeto a validar
     * @throws ProductsException
     *             En caso de incumplir alguna de las validaciones
     */
    public void validate(Object o) throws ProductsException {
        var violations = this.validator.validate(o);
        this.throwExceptionIfExistsViolations(violations);
    }

    /**
     * Ejecuta la validacion de Beans con Hibernate Validation y lanza una excepcion con las violaciones encontradas. Ejecuta solo las
     * validaciones correspondientes a los grupos indicados en la invocacion
     *
     * @param o
     *            Objeto a validar
     * @param groups
     *            Grupo de restricciones que deben ser validadas
     * @throws ProductsException
     *             En caso de incumplir alguna de las validaciones
     */
    public void validate(Object o, Class<?>... groups) throws ProductsException {
        var violations = this.validator.validate(o, groups);
        this.throwExceptionIfExistsViolations(violations);
    }

    /**
     * Metodo que valida si existe alguna violacion, y de ser el caso lanza la excepcion correspondiente
     *
     * @param violations
     *            Lista con el resultado de la validacion
     */
    private void throwExceptionIfExistsViolations(Set<ConstraintViolation<Object>> violations) {
        if (!violations.isEmpty()) {
            throw new ProductsException(HttpStatus.BAD_REQUEST.value(), UtilConstants.DAE000,
                    this.messagesUtil.getMessage(UtilConstants.DAE000),
                    violations.stream().map(this::toFieldError).collect(Collectors.toList()));
        }
    }

    /**
     * Metodo que permite construir el objeto de respuesta generico a partir de una lista de violaciones
     *
     * @param violations
     *            Lista de violaciones
     * @return Objeto de respuesta generico, con la informacion de las violaciones encontradas durante el proceso de validacion
     */
    public ResponseDto<Void> createResponseFromConstraintViolations(Set<ConstraintViolation<?>> violations) {
        return new ResponseDto<>(HttpStatus.BAD_REQUEST.value(), UtilConstants.DAE001,
                this.messagesUtil.getMessage(UtilConstants.DAE001),
                violations.stream().map(this::toFieldError).collect(Collectors.toList()));
    }

    /**
     * Transforma una violacion hacia el Dto que permite visualizar el campo en el cual se incumple la validacion y el mensaje
     * correspondiente
     *
     * @param violation
     *            Objeto con la informacion de la violacion
     * @return DTO para los errores en campos
     */
    public FieldErrorDto toFieldError(ConstraintViolation<?> violation) {
        String code = violation.getMessage();
        String message;
        if (code.equals(UtilConstants.DAE002)) {
            message = this.messagesUtil.getFormatedMessage(code, violation.getConstraintDescriptor().getAttributes().get("value"));
        } else if (code.equals(UtilConstants.DAE003)) {
            message = this.messagesUtil.getFormatedMessage(code, violation.getConstraintDescriptor().getAttributes().get("max"));
        } else {
            message = this.messagesUtil.getMessage(code);
        }
        var propertyPath = violation.getPropertyPath().toString();
        return new FieldErrorDto(propertyPath.substring(propertyPath.lastIndexOf('.') + 1), message);
    }

    public ResponseDto<Void> createResponseFromFieldErrors(List<FieldError> errors) {
        return new ResponseDto<>(HttpStatus.BAD_REQUEST.value(), UtilConstants.DAE000,
                this.messagesUtil.getMessage(UtilConstants.DAE000));
    }

}
