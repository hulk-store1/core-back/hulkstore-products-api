package com.hulkstore.productsapi.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.ResourceBundle;

@Component
@Slf4j
public class MessagesUtil {

    private static final String MESSAGE_PROPERTIES = "messages/messages";

    public String getMessage(String code) {
        try {
            var bundle = ResourceBundle.getBundle(MESSAGE_PROPERTIES);
            return bundle.getString(code);
        } catch (Exception e) {
            log.error("Error loading message property: {}", code);
            return "";
        }
    }

    public String getFormatedMessage(String code, Object... params) {
        try {
            var bundle = ResourceBundle.getBundle(MESSAGE_PROPERTIES);
            return MessageFormat.format(bundle.getString(code), params);
        } catch (Exception e) {
            log.error("Error loading message property: {}", code);
            return "";
        }
    }

}
