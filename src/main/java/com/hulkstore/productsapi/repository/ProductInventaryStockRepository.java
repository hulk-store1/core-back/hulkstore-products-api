package com.hulkstore.productsapi.repository;

import com.hulkstore.productsapi.model.Product;
import com.hulkstore.productsapi.model.ProductInventaryStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductInventaryStockRepository extends JpaRepository<ProductInventaryStock, Integer> {

    List<ProductInventaryStock> findByProductInventaryExistQty(Integer i);
    List<ProductInventaryStock> findByProduct(Product product);
    List<ProductInventaryStock> findByProductOrderByProductInventaryLastUpdateDesc(Product product);
}
