package com.hulkstore.productsapi;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(info = @Info(
		title = "B2C Order API", description = "Microservicio que expone las operaciones para creación de ordenes en el e-commerce APP B2C", version = "1.0.0",
		contact = @Contact(
				email = "diego.gomez@pragma.com.co", name = "Pragma S.A", url = "www.pragma.com.co"),
		license = @License(name = "MIT", url = "https://opensource.org/licenses/MIT")),
		servers = {
				/*@Server(description = "Develop", url = "https://h73euwasy3.execute-api.us-east-1.amazonaws.com/dev/b2c/authentication")*/
				@Server(description = "Local", url = "http://localhost:8108/b2c/authentication")
		})
@SpringBootApplication
public class ProductsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsApiApplication.class, args);
	}

}
