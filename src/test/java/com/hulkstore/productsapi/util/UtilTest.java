package com.hulkstore.productsapi.util;

import com.hulkstore.productsapi.domain.MovementKind;
import com.hulkstore.productsapi.model.Product;
import com.hulkstore.productsapi.model.ProductInventaryStock;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class UtilTest {

    public static List<Product> getAllProducts1() {
        Product product1 = new Product();
        product1.setProductId(1);
        product1.setProductName("Camiseta 1");
        product1.setProductImage("https://frutocuatro.com/wp-content/uploads/2018/05/camiseta-64000-royal-frente.jpg");
        Product product2 = new Product();
        product2.setProductId(2);
        product2.setProductName("Camiseta 2");
        product2.setProductImage("https://chevignon.vtexassets.com/arquivos/ids/744526/63_6308000_041300_0.jpg?v=637590481205130000");
        Product product3 = new Product();
        product3.setProductId(3);
        product3.setProductName("Camiseta 3");
        product3.setProductImage("https://cf.shopee.com.co/file/5fee2794dbfe024fcf26ab4fd9e52d64_tn");
        List<Product> products = Arrays.asList(product1, product2, product3);
        return products;
    }

    public static Optional<Product> getProduct1() {
        Product product1 = new Product();
        product1.setProductId(1);
        product1.setProductName("Camiseta 1");
        product1.setProductImage("https://frutocuatro.com/wp-content/uploads/2018/05/camiseta-64000-royal-frente.jpg");
        return Optional.of(product1);
    }

    public static List<ProductInventaryStock> getProductInventary1() {
        ProductInventaryStock productInventaryStock1 = new ProductInventaryStock();
        productInventaryStock1.setProductInventaryId(62);
        productInventaryStock1.setProduct(getProduct1().get());
        productInventaryStock1.setProductInventaryDateCreated(LocalDateTime.now());
        productInventaryStock1.setProductInventaryLastUpdate(LocalDateTime.now());
        productInventaryStock1.setMovementKind(MovementKind.Sale);
        productInventaryStock1.setProductInventaryExistQty(10);
        productInventaryStock1.setProductInventaryExistUnitPrice(BigDecimal.valueOf(60000));
        productInventaryStock1.setProductInventaryExistTotal(BigDecimal.valueOf(600000));
        productInventaryStock1.setProductInventaryOutflowQty(1);
        productInventaryStock1.setProductInventaryOutflowUnitPrice(BigDecimal.valueOf(60000));
        productInventaryStock1.setProductInventaryOutflowTotal(BigDecimal.valueOf(60000));

        ProductInventaryStock productInventaryStock2 = new ProductInventaryStock();
        productInventaryStock2.setProductInventaryId(61);
        productInventaryStock2.setProduct(getProduct1().get());
        productInventaryStock2.setProductInventaryDateCreated(LocalDateTime.now());
        productInventaryStock2.setProductInventaryLastUpdate(LocalDateTime.now());
        productInventaryStock2.setMovementKind(MovementKind.InitialInventary);
        productInventaryStock2.setProductInventaryExistQty(10);
        productInventaryStock2.setProductInventaryExistUnitPrice(BigDecimal.valueOf(60000));
        productInventaryStock2.setProductInventaryExistTotal(BigDecimal.valueOf(600000));
        return Arrays.asList(productInventaryStock1, productInventaryStock2);
    }

    public static ProductInventaryStock getProductInventarySaved1() {
        ProductInventaryStock productInventaryStock1 = new ProductInventaryStock();
        productInventaryStock1.setProductInventaryId(63);
        productInventaryStock1.setProduct(getProduct1().get());
        productInventaryStock1.setProductInventaryDateCreated(LocalDateTime.now());
        productInventaryStock1.setProductInventaryLastUpdate(LocalDateTime.now());
        productInventaryStock1.setMovementKind(MovementKind.Sale);
        productInventaryStock1.setProductInventaryExistQty(9);
        productInventaryStock1.setProductInventaryExistUnitPrice(BigDecimal.valueOf(60000));
        productInventaryStock1.setProductInventaryExistTotal(BigDecimal.valueOf(540000));
        productInventaryStock1.setProductInventaryOutflowQty(1);
        productInventaryStock1.setProductInventaryOutflowUnitPrice(BigDecimal.valueOf(60000));
        productInventaryStock1.setProductInventaryOutflowTotal(BigDecimal.valueOf(60000));
        return productInventaryStock1;
    }

}
