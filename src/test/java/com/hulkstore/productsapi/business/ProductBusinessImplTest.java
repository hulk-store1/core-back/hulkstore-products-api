package com.hulkstore.productsapi.business;

import com.hulkstore.productsapi.domain.ProductInventaryStockDto;
import com.hulkstore.productsapi.domain.ProductOutflowDto;
import com.hulkstore.productsapi.domain.ProductsException;
import com.hulkstore.productsapi.model.Product;
import com.hulkstore.productsapi.repository.ProductInventaryStockRepository;
import com.hulkstore.productsapi.repository.ProductRepository;
import com.hulkstore.productsapi.util.MessagesUtil;
import com.hulkstore.productsapi.util.UtilConstants;
import com.hulkstore.productsapi.util.UtilTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class ProductBusinessImplTest {

    @InjectMocks ProductBusinessImpl productBusinessImpl;

    @Mock ProductRepository productRepository;
    @Mock
    ProductInventaryStockRepository productInventaryStockRepository;
    @Mock
    MessagesUtil messagesUtil;

    @Test
    void getProductsOK() {

        String token = "tokentest";
        when(productRepository.findAll()).thenReturn(UtilTest.getAllProducts1());
        var response = productBusinessImpl.getProducts(token);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(UtilConstants.DAA003, response.getBody().getResponseCode());
        assertEquals(UtilTest.getAllProducts1().size(), response.getBody().getData().size());
        assertEquals(UtilTest.getAllProducts1().get(0).getProductId(), response.getBody().getData().get(0).getProductId());
    }

    @Test
    void getEmptyProductList() {

        String token = "tokentest";
        when(productRepository.findAll()).thenReturn(List.of());
        var exception = assertThrows(ProductsException.class, () -> productBusinessImpl.getProducts(token));

        assertNotNull(exception);
        assertEquals(HttpStatus.NOT_FOUND.value(), exception.getStatus());
        assertEquals(UtilConstants.DAE012, exception.getCode());
    }

    @Test
    void buyProductOK() {

        String token = "tokentest";

        ProductOutflowDto productOutflowDto = new ProductOutflowDto();
        productOutflowDto.setOutFlowDto(new ProductInventaryStockDto.OutFlowDto(1, null, null));

        Optional<Product> productReturned = UtilTest.getProduct1();
        when(productRepository.findById(eq(1))).thenReturn(productReturned);
        when(productInventaryStockRepository.findByProductOrderByProductInventaryLastUpdateDesc(eq(productReturned.get())))
                .thenReturn(UtilTest.getProductInventary1());
        when(productInventaryStockRepository.save(any())).thenReturn(UtilTest.getProductInventarySaved1());

        var response = productBusinessImpl.createOutflowInventaryKardex( token,"1", productOutflowDto);

        assertNotNull(response);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(9, response.getBody().getData().getProductInventaryExistQty());
        // assertEquals(UtilConstants.DAE012, exception.getCode());
    }

    @Test
    void buyProductWithoutInventary() {

        String token = "tokentest";

        ProductOutflowDto productOutflowDto = new ProductOutflowDto();
        productOutflowDto.setOutFlowDto(new ProductInventaryStockDto.OutFlowDto(100, null, null));

        Optional<Product> productReturned = UtilTest.getProduct1();
        when(productRepository.findById(eq(1))).thenReturn(productReturned);
        when(productInventaryStockRepository.findByProductOrderByProductInventaryLastUpdateDesc(eq(productReturned.get())))
                .thenReturn(UtilTest.getProductInventary1());
        when(productInventaryStockRepository.save(any())).thenReturn(UtilTest.getProductInventarySaved1());

        var exception = assertThrows(ProductsException.class, () -> productBusinessImpl.createOutflowInventaryKardex( token,"1", productOutflowDto));

        assertNotNull(exception);
        assertEquals(HttpStatus.BAD_REQUEST.value(), exception.getStatus());
        assertEquals(UtilConstants.DAE011, exception.getCode());
    }





}